import VueIcon from './IconBase';
import { IconBaseProps } from './Icon';
import { IconDefinition } from '@xrhcc-ui/icons-svg/lib/types';
import { getTwoToneColor, TwoToneColor, setTwoToneColor } from './twoTonePrimaryColor';
import { normalizeTwoToneColors } from '../utils';
import { SetupContext } from 'vue';

export interface XrhccIconProps extends IconBaseProps {
  twoToneColor?: TwoToneColor;
}

export interface IconComponentProps extends XrhccIconProps {
  icon: IconDefinition;
}

// Initial setting
setTwoToneColor('#1890ff');

const Icon = (props: IconComponentProps, context: SetupContext) => {
  const {
    class: cls,
    icon,
    spin,
    rotate,
    tabindex,
    // other
    twoToneColor,
    onClick,
    ...restProps
  } = { ...props, ...context.attrs } as any;
  const classObj = {
    xrhccicon: true,
    [`xrhccicon-${icon.name}`]: Boolean(icon.name),
    [cls]: cls,
  };

  const svgClassString = !!spin || icon.name === 'loading' ? 'xrhccicon-spin' : '';

  let iconTabIndex = tabindex;
  if (iconTabIndex === undefined && onClick) {
    iconTabIndex = -1;
    restProps.tabindex = iconTabIndex;
  }

  const svgStyle = rotate
    ? {
        msTransform: `rotate(${rotate}deg)`,
        transform: `rotate(${rotate}deg)`,
      }
    : undefined;
  const [primaryColor, secondaryColor] = normalizeTwoToneColors(twoToneColor);

  return (
    <span role="img" aria-label={icon.name} {...restProps} onClick={onClick} class={classObj}>
      <VueIcon
        class={svgClassString}
        icon={icon}
        primaryColor={primaryColor}
        secondaryColor={secondaryColor}
        style={svgStyle}
      />
    </span>
  );
};

Icon.displayName = 'XrhccIcon';
Icon.inheritAttrs = false;
Icon.getTwoToneColor = getTwoToneColor;
Icon.setTwoToneColor = setTwoToneColor;

export default Icon;
