// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import { SetupContext } from 'vue';
import AccountBookFilledSvg from '@xrhcc-ui/icons-svg/lib/asn/AccountBookEditor';
import XrhccIcon, { XrhccIconProps } from '../components/XrhccIcon';

const AccountBookFilled = (props: XrhccIconProps, context: SetupContext) => {
  const p = { ...props, ...context.attrs };
  return <XrhccIcon {...p} icon={AccountBookFilledSvg}></XrhccIcon>;
};

AccountBookFilled.displayName = 'AccountBookFilled';
AccountBookFilled.inheritAttrs = false;
export default AccountBookFilled;