import { createApp } from 'vue';
import * as XrhccIcons from '../src/icons';

const SimpleDemo = {
  mounted() {
    console.log(XrhccIcons);
  },
  render() {
    return null;
  },
};

createApp(SimpleDemo).mount('#__vue-content>div');
