<h1 align="center">
Xrhcc Icons for Vue
</h1>

## Install

```bash
yarn add @xrhcc-ui/icons-vue
```

## Basic Usage

First, you should add the icons that you need into the library.

```js
import Vue from 'vue'
import { Filter } from '@xrhcc-ui/icons-vue';
Vue.component(Filter.name, Filter);
```

After that, you can use xrhcc icons in your Vue components as simply as this:

```jsx
<icon-filter />
```

## Build project

```bash
npm run generate # Generate files to ./src
npm run compile # Build library
npm run test # Runing Test
```
