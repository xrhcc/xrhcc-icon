<h1 align="center">
Xrhcc Icons for Angular
</h1>


## Installation

```bash
ng add @xrhcc-ui/icons-angular

# or npm install @xrhcc-ui/icons-angular
```

## Usage

You should import `IconModule` in your application's root module.

```ts
import { IconModule } from '@xrhcc-ui/icons-angular';

@NgModule({
  imports: [
    IconModule
  ]
})
export class AppModule { }
```

And register the icons that you need to `IconService` (All or explicitly. We call it **static loading**):

> ATTENTION! We strongly suggest you not to register all icons. That would increase your bundle's size dramatically.

```ts
import { Component, OnInit } from '@angular/core';
import { IconDefinition, IconService } from '@xrhcc-ui/icons-angular';
import { AccountBookFill } from '@xrhcc-ui/icons-angular/icons'

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private _iconService: IconService) {
    // Import all. NOT RECOMMENDED. ❌
    // const xrhccIcons = AllIcons as {
      // [key: string]: IconDefinition;
    // };
    // this._iconService.addIcon(...Object.keys(xrhccIcons).map(key => xrhccIcons[key]));
    // Import what you need! ✔️
    this._iconService.addIcon(...[ AccountBookFill ]);
    this._iconService.twoToneColor = { primaryColor: '#1890ff' };
  }
}
```

When you want to render an icon:

```html
<i xrhccicon type="ant-cloud" theme="outline"></i>
```


## Development

This package, unlike `@xrhcc-ui/icons-react`, does not list `@xrhcc-ui/icons` as a dependency. It has its own build up tooling chain which brings benefits like:

* **Tree shake**.
* Providing dynamic and static loading.
* Reduced bundle size (500KB less if you only use dynamic loading).
* Better performance because of no `svg => abstract node => svg` process.

### Setup

Install dependencies of `@xrhcc-ui/icons-angular`, and run `npm run generate`.

### Demo

Run `ng serve` after `npm run generate`.

### Build

Build the library by running the script we provide.

```bash
$ ./build.sh
```

### Extension

You can simply extend this package by creating directives or services that extends `IconDirective` and `IconService`. And it is worth mentioning that `_changeIcon` method returns a `Promise<svg>` using which you could add extra modifications. [ng-zorro-antd](https://github.com/NG-ZORRO/ng-zorro-antd/tree/master/components/icon) is a good example of extending this package.
