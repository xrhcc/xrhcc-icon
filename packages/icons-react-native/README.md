<h1 align="center">
Xrhcc Icons for React Native
</h1>


## Install

```bash
yarn add xrhcc-ui/icons-react-native
```

## Linking

```bash
react-native link xrhcc-ui/icons-react-native
```

will copy fonts to `ios` and `android` assets folder.

After linking succeeded you need run your app from `xcode` or `android studio` or start it from terminal `react-native run-ios` or `react-native run-android`

## Basic Usage

```tsx
import { IconFill, IconOutline } from "xrhcc-ui/icons-react-native";
```

After that, you can use xrhcc icons in your React components as simply as this:

```jsx
<IconFill name="account-book" />
<IconOutline name="account-book" />
```

## Component Interface

```ts
interface IconFillProps extends TextProps {
  name: FillGlyphMapType;
  size?: number;
  color?: string;
}
```
