import {series, parallel, task} from 'gulp';
import {
  clean,
  copy,
  generateEntry,
  generateInline
} from './tasks/creators';
import {IconDefinition} from './templates/types';
import {ExtractRegExp} from './tasks/creators/generateInline';

import tasks from './gulpfileInit'
export default series(
  // 1. clean
  clean(['src', 'inline-svg', 'es', 'lib']),

  parallel(
    copy({
      from: ['templates/*.ts'],
      toDir: 'src'
    }),

    parallel(
      tasks
    )
    // 2.2 generate abstract node with the theme "base"
    // generateIconsByDir("svg"),

    // 2.2 generate abstract node with the theme "outlined"
    /*generateIcons({
      theme: 'outlined',
      from: ['svg/editor/!*.svg'],
      toDir: 'src/asn',
      svgoConfig: generalConfig,
      extraNodeTransformFactories: [
        assignAttrsAtTag('svg', {focusable: 'false'}),
        adjustViewBox
      ],
      stringify: JSON.stringify,
      template: iconTemplate,
      mapToInterpolate: ({name, content}) => ({
        identifier: getIdentifier({name, themeSuffix: 'Editor'}),
        content
      }),
      filename: ({name}) => getIdentifier({name, themeSuffix: 'Editor'})
    }),*/

    // 2.3 generate abstract node with the theme "outlined"
    /*generateIcons({
      theme: 'outlined',
      from: ['svg/system/!*.svg'],
      toDir: 'src/asn',
      svgoConfig: generalConfig,
      // svgoConfig: remainFillConfig,
      extraNodeTransformFactories: [
        assignAttrsAtTag('svg', {focusable: 'false'}),
        adjustViewBox,
        setDefaultColorAtPathTag('#333')
      ],
      // stringify: twotoneStringify,
      stringify: JSON.stringify,
      template: iconTemplate,
      mapToInterpolate: ({name, content}) => ({
        identifier: getIdentifier({name, themeSuffix: 'System'}),
        content
      }),
      filename: ({name}) => getIdentifier({name, themeSuffix: 'System'})
    })*/
  ),
  parallel(
    // 3.1 generate entry file: src/index.ts
    generateEntry({
      entryName: 'index.ts',
      from: ['src/asn/*.ts'],
      toDir: 'src',
      banner: '// This index.ts file is generated automatically.\n',
      template: `export { default as <%= identifier %> } from '<%= path %>';`,
      mapToInterpolate: ({name: identifier}) => ({
        identifier,
        path: `./asn/${identifier}`
      })
    }),
    // 3.2 generate inline SVG files
    generateInline({
      from: ['src/asn/*.ts'],
      toDir: ({_meta}) => `inline-svg/${_meta && _meta.theme}`,
      getIconDefinitionFromSource: (content: string): IconDefinition => {
        const extract = ExtractRegExp.exec(content);
        if (extract === null || !extract[1]) {
          throw new Error('Failed to parse raw icon definition: ' + content);
        }
        return new Function(`return ${extract[1]}`)() as IconDefinition;
      }
    }),
    // 3.3 generate inline SVG files with namespace
    generateInline({
      from: ['src/asn/*.ts'],
      toDir: ({_meta}) => `inline-namespaced-svg/${_meta && _meta.theme}`,
      getIconDefinitionFromSource: (content: string): IconDefinition => {
        const extract = ExtractRegExp.exec(content);
        if (extract === null || !extract[1]) {
          throw new Error('Failed to parse raw icon definition: ' + content);
        }
        return new Function(`return ${extract[1]}`)() as IconDefinition;
      },
      renderOptions: {
        extraSVGAttrs: {xmlns: 'http://www.w3.org/2000/svg'}
      }
    })
  )
);
