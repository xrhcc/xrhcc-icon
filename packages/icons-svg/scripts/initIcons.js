const fs = require('fs');

function getFilesAndFoldersInDir(path) {
  const items = fs.readdirSync(path);
  const result = [];
  items.forEach(item => {
    const itemPath = `${path}/${item}`;
    const stat = fs.statSync(itemPath);
    if (stat.isDirectory()) {
      let data = {
        // 文件夹
        path: itemPath + "/*.svg",
        name: item,
        themeSuffix: item.charAt(0).toUpperCase() + item.slice(1)
      }
      // console.log("'" + data.themeSuffix + "' |")
      // console.log("<option value=\"" + data.themeSuffix + "\">" + data.themeSuffix + "</option>")
      result.push(data);
    } else {
      // 文件
    }
  });
  return result;
}

function clearFile(filename) {
// 写入文件是异步过程，需要使用promise保证文件操作完成
  return new Promise(resolve => {
    fs.writeFile(filename, '', "utf-8", function (err) {
      if (err) {
        throw new Error("写入数据失败");
      } else {
        resolve()
      }
    });
  })
}


clearFile('gulpfileInit.ts')

var parallels = []
fs.appendFileSync('gulpfileInit.ts', `
import {series, parallel, task} from 'gulp';
import {
  generateIcons,
} from './tasks/creators';
import {
  assignAttrsAtTag,
} from './plugins/svg2Definition/transforms';
import {readFileSync} from 'fs';
import {resolve} from 'path';
import {getIdentifier} from './utils';
import { getGeneralConfig} from "./plugins/svgo/presets/general";

const iconTemplate = readFileSync(
  resolve(__dirname, './templates/icon.ts.ejs'),
  'utf8'
);
`);

const result = getFilesAndFoldersInDir("svg")
for (const index in result) {
  let data = result[index]

  let svg = `
task('${data.themeSuffix}', generateIcons({
  theme: 'noFilled',
  from: ['${data.path}'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: '${data.themeSuffix}'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: '${data.themeSuffix}'})
}))
`
  fs.appendFileSync("gulpfileInit.ts", svg);
  parallels.push(data.themeSuffix)
}

fs.appendFileSync("gulpfileInit.ts", `

export default ${JSON.stringify(parallels)}
`);
