# 贡献指南

`@xrhcc-ui/icons-svg` 是语义化矢量图形库。该库提供了描述图标的抽象节点（Abstract Node, ASN）来满足各种框架的基本适配。

## 目录结构

```bash
.
├── ReadMe.md
├── babel.config.cjs      # 用于测试环境的 babel 配置
├── docs                  # 项目文档
├── es                    # tsc 输出目录，使用 ES Modules
├── gulpfile.ts           # 用于整个图标生成的 gulp 配置文件
├── inline-svg            # gulp 输出目录，输出可直接使用的 SVG
├── inline-namespaced-svg # gulp 输出目录，输出可直接使用的 SVG
├── jest.config.cjs
├── lib                   # tsc 输出目录，使用 CommonJS
├── package.json
├── plugins               # gulp 的插件目录，存放用于处理图标的自定义 gulp 插件
├── scripts               # 其他自定义的脚本文件
├── src                   # 源文件目录，该目录下的文件通常由 gulp 生成，然后通过 tsc 编译输出到 es、lib 目录
├── svg                   # 存放从设计师得到的，未经处理的图标文件
├── tasks/creators        # gulp 的任务工厂函数目录，存放用于处理图标的自定义 gulp 任务工厂函数
├── templates             # 模板目录
├── test
├── tsconfig.build.json   # 用于编译构建（build）的 TypeScript 配置
├── tsconfig.json         # 用于生成（generate）的（gulp 运行环境） TypeScript 配置
├── typings.d.ts
└── utils
```

## 生成流程

通过运行命令

```bash
yarn generate # or `yarn g`
```


## 编译流程

通过运行命令

```bash
yarn build
```

使用 `tsc` 编译输出至 `es`、`lib` 目录

## 如何新增/修改/删除图标

### 新增

首先确保图标符合以下设计上的要求：

1. 图标的 `viewBox` 为 `0 0 1024 1024`

2. 图标外围有预留 `64px` 的出血位

然后将需要新增的图标添加至 `svg` 目录下对应的主题风格目录中，即 `filled`, `outlined`, `twotone` 中的一个

运行目录下的 `npm scripts`

```bash
# 生成 ts 源文件至 src
yarn generate

# 编译 src 文件至 es、lib
yarn build
```

### 修改/删除

修改和删除跟新增图标类似，直接修改/删除 `svg` 目录下对应主题风格目录中的 `.svg` 图标即可
