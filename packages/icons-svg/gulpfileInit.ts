
import {series, parallel, task} from 'gulp';
import {
  generateIcons,
} from './tasks/creators';
import {
  assignAttrsAtTag,
} from './plugins/svg2Definition/transforms';
import {readFileSync} from 'fs';
import {resolve} from 'path';
import {getIdentifier} from './utils';
import { getGeneralConfig} from "./plugins/svgo/presets/general";

const iconTemplate = readFileSync(
  resolve(__dirname, './templates/icon.ts.ejs'),
  'utf8'
);

task('Antd', generateIcons({
  theme: 'noFilled',
  from: ['svg/antd/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Antd'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Antd'})
}))

task('Assets', generateIcons({
  theme: 'noFilled',
  from: ['svg/assets/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Assets'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Assets'})
}))

task('Base', generateIcons({
  theme: 'noFilled',
  from: ['svg/base/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Base'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Base'})
}))

task('Chain', generateIcons({
  theme: 'noFilled',
  from: ['svg/chain/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Chain'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Chain'})
}))

task('Chart', generateIcons({
  theme: 'noFilled',
  from: ['svg/chart/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Chart'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Chart'})
}))

task('Data', generateIcons({
  theme: 'noFilled',
  from: ['svg/data/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Data'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Data'})
}))

task('Date', generateIcons({
  theme: 'noFilled',
  from: ['svg/date/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Date'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Date'})
}))

task('Direction', generateIcons({
  theme: 'noFilled',
  from: ['svg/direction/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Direction'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Direction'})
}))

task('Editor', generateIcons({
  theme: 'noFilled',
  from: ['svg/editor/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Editor'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Editor'})
}))

task('Flow', generateIcons({
  theme: 'noFilled',
  from: ['svg/flow/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Flow'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Flow'})
}))

task('Gesture', generateIcons({
  theme: 'noFilled',
  from: ['svg/gesture/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Gesture'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Gesture'})
}))

task('Graphics', generateIcons({
  theme: 'noFilled',
  from: ['svg/graphics/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Graphics'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Graphics'})
}))

task('Hardware', generateIcons({
  theme: 'noFilled',
  from: ['svg/hardware/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Hardware'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Hardware'})
}))

task('Health', generateIcons({
  theme: 'noFilled',
  from: ['svg/health/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Health'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Health'})
}))

task('Icons', generateIcons({
  theme: 'noFilled',
  from: ['svg/icons/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Icons'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Icons'})
}))

task('Link', generateIcons({
  theme: 'noFilled',
  from: ['svg/link/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Link'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Link'})
}))

task('Office', generateIcons({
  theme: 'noFilled',
  from: ['svg/office/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Office'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Office'})
}))

task('Other', generateIcons({
  theme: 'noFilled',
  from: ['svg/other/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Other'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Other'})
}))

task('Safe', generateIcons({
  theme: 'noFilled',
  from: ['svg/safe/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Safe'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Safe'})
}))

task('Screen', generateIcons({
  theme: 'noFilled',
  from: ['svg/screen/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Screen'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Screen'})
}))

task('Symbol', generateIcons({
  theme: 'noFilled',
  from: ['svg/symbol/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Symbol'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Symbol'})
}))

task('Telecom', generateIcons({
  theme: 'noFilled',
  from: ['svg/telecom/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Telecom'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Telecom'})
}))

task('User', generateIcons({
  theme: 'noFilled',
  from: ['svg/user/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'User'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'User'})
}))

task('Weather', generateIcons({
  theme: 'noFilled',
  from: ['svg/weather/*.svg'],
  toDir: 'src/asn',
  svgoConfig: getGeneralConfig('noFilled'),
  extraNodeTransformFactories: [
    assignAttrsAtTag('svg', {focusable: 'false', theme: 'noFilled'}),
  ],
  stringify: JSON.stringify,
  template: iconTemplate,
  mapToInterpolate: ({name, content}) => ({
    identifier: getIdentifier({name, themeSuffix: 'Weather'}),
    content
  }),
  filename: ({name}) => getIdentifier({name, themeSuffix: 'Weather'})
}))


export default ["Antd","Assets","Base","Chain","Chart","Data","Date","Direction","Editor","Flow","Gesture","Graphics","Hardware","Health","Icons","Link","Office","Other","Safe","Screen","Symbol","Telecom","User","Weather"]
