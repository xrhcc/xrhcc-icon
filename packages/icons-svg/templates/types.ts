export interface AbstractNode {
  tag: string;
  attrs: {
    [key: string]: string;
  };
  children?: AbstractNode[];
}

export interface IconDefinition {
  name: string; // kebab-case-style
  theme: ThemeType;
  icon:
    | ((primaryColor: string, secondaryColor: string) => AbstractNode)
    | AbstractNode;
}

export type ThemeType = 'noFilled' | 'filled';
export type ThemeTypeUpperCase =
  'Antd' |
  'Assets' |
  'Base' |
  'Chart' |
  'Chain' |
  'Data' |
  'Date' |
  'Direction' |
  'Editor' |
  'Flow' |
  'Gesture' |
  'Graphics' |
  'Hardware' |
  'Health' |
  'Icons' |
  'Link' |
  'Office' |
  'Other' |
  'Safe' |
  'Screen' |
  'Symbol' |
  'Telecom' |
  'User' |
  'Weather' |
  any
  ;
