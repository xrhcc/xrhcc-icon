import SVGO from 'svgo';
import {mergeRight} from 'ramda';
import {base} from './base';

export const generalConfig: SVGO.Options = mergeRight(base, {
  plugins: [
    ...(base.plugins || []),
    {removeAttrs: {attrs: ['class', 'fill']}}
  ]
});

export const generalConfigNoFilled: SVGO.Options = mergeRight(base, {
  plugins: [
    ...(base.plugins || []),
    {removeAttrs: {attrs: ['class']}}
  ]
});

export const getGeneralConfig = (theme: string) => {
  let generalConfig = {}
  if (theme === 'noFilled') {
    generalConfig = mergeRight(base, {
      plugins: [
        ...(base.plugins || []),
        {removeAttrs: {attrs: ['class']}}
      ]
    })
  } else {
    generalConfig = mergeRight(base, {
      plugins: [
        ...(base.plugins || []),
        {removeAttrs: {attrs: ['class', 'fill']}}
      ]
    });
  }

  return generalConfig
}
