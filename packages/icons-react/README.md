<h1 align="center">
Xrhcc Icons for React
</h1>


## Install

```bash
yarn add @xrhcc-ui/icons@1.0.0
```

## Basic Usage

You can import it directly or destructure from `@xrhcc-ui/icons` when tree-shaking enabled.

```ts
import SmileOutlined from '@xrhcc-ui/icons/SmileOutlined';
import { SmileOutlined } from '@xrhcc-ui/icons';

import SmileFilled from '@xrhcc-ui/icons/SmileFilled';
import SmileTwoTone from '@xrhcc-ui/icons/SmileTwoTone';
import { SmileFilled, SmileTwoTone } from '@xrhcc-ui/icons';
```

## Component Interface

```ts
interface XrhccIconProps {
  className?: string;
  onClick?: React.MouseEventHandler<SVGSVGElement>;
  style?: React.CSSProperties;
}
```

## Release

```bash
npm run generate
npm run compile
npm publish
```
