import {AbstractNode, IconDefinition} from '@xrhcc-ui/icons-svg/lib/types';
import {generate as generateColor} from '@ant-design/colors';
import React, {useEffect} from 'react';
import warn from 'rc-util/lib/warning';
import {insertCss} from 'insert-css';

export function warning(valid: boolean, message: string) {
    warn(valid, `[@xrhcc-ui/icons] ${message}`);
}

export function isIconDefinition(target: any): target is IconDefinition {
    return (
        typeof target === 'object' &&
        typeof target.name === 'string' &&
        typeof target.theme === 'string' &&
        (typeof target.icon === 'object' || typeof target.icon === 'function')
    );
}

export function normalizeAttrs(attrs: Attrs = {}): Attrs {
    return Object.keys(attrs).reduce((acc: Attrs, key) => {
        const val = attrs[key];
        // console.log("normalizeAttrs -- ", key, val)
        switch (key) {
            case 'class':
                acc.className = val;
                delete acc.class;
                break;
            case 'stroke-linejoin':
                acc.strokeLinejoin = val;
                delete acc['stroke-linejoin'];
                break;
            case 'stroke-width':
                acc.strokeWidth = val;
                delete acc['stroke-width'];
                break;
            case 'stroke-linecap':
                acc.strokeLinecap = val;
                delete acc['stroke-linecap'];
                break;
            case 'fill-rule':
                acc.fillRule = val;
                delete acc['fill-rule'];
                break;
            case 'clip-path':
                acc.clipPath = val;
                delete acc['clip-path'];
                break;
            case 'clip-rule':
                acc.clipRule = val;
                delete acc['clip-rule'];
                break;
            case 'stroke-miterlimit':
                acc.strokeMiterlimit = val;
                delete acc['stroke-miterlimit'];
                break;


            default:
                acc[key] = val;
        }
        return acc;
    }, {});
}

export interface Attrs {
    [key: string]: string;
}

export function generate(
    node: AbstractNode,
    key: string,
    rootProps?: { [key: string]: any } | false,
): any {
    if (!rootProps) {
        return React.createElement(
            node.tag,
            {key, ...normalizeAttrs(node.attrs)},
            (node.children || []).map((child, index) => generate(child, `${key}-${node.tag}-${index}`)),
        );
    }
    return React.createElement(
        node.tag,
        {
            key,
            ...normalizeAttrs(node.attrs),
            ...rootProps,
        },
        (node.children || []).map((child, index) => generate(child, `${key}-${node.tag}-${index}`)),
    );
}

export function getSecondaryColor(primaryColor: string): string {
    // choose the second color
    return generateColor(primaryColor)[0];
}

export function normalizeTwoToneColors(
    twoToneColor: string | [string, string] | undefined,
): string[] {
    if (!twoToneColor) {
        return [];
    }

    return Array.isArray(twoToneColor) ? twoToneColor : [twoToneColor];
}

// These props make sure that the SVG behaviours like general text.
// Reference: https://blog.prototypr.io/align-svg-icons-to-text-and-say-goodbye-to-font-icons-d44b3d7b26b4
export const svgBaseProps = {
    width: '1em',
    height: '1em',
    // fill: 'currentColor',
    'aria-hidden': 'true',
    focusable: 'false',
};

export const iconStyles = `
.ant-btn > .xrhccicon {
   margin-inline-end: 8px;
}

.xrhccicon {
  display: inline-block;
  color: inherit;
  font-style: normal;
  line-height: 0;
  text-align: center;
  text-transform: none;
  vertical-align: -0.125em;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.xrhccicon > * {
  line-height: 1;
}

.xrhccicon svg {
  display: inline-block;
}

.xrhccicon::before {
  display: none;
}

.xrhccicon .xrhccicon-icon {
  display: block;
}

.xrhccicon[tabindex] {
  cursor: pointer;
}

.xrhccicon-spin::before,
.xrhccicon-spin {
  display: inline-block;
  -webkit-animation: loadingCircle 1s infinite linear;
  animation: loadingCircle 1s infinite linear;
}

@-webkit-keyframes loadingCircle {
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}

@keyframes loadingCircle {
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
`;

let cssInjectedFlag = false;

export const useInsertStyles = (styleStr: string = iconStyles) => {
    useEffect(() => {
        if (!cssInjectedFlag) {
            insertCss(styleStr, {
                prepend: true,
            });
            cssInjectedFlag = true;
        }
    }, []);
};
