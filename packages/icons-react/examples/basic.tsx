import * as React from 'react';
import {AccountBookFilled} from '../src';

const Basic = () => (
    <div>
        <AccountBookFilled/>
        <AccountBookFilled/>
        <AccountBookFilled/>
        <AccountBookFilled spin/>
        <AccountBookFilled rotate={180}/>
        <AccountBookFilled/>
        <AccountBookFilled onMouseDown={() => console.log('mouse down')}/>
        <AccountBookFilled onKeyUp={() => console.log('key up')}/>
        <AccountBookFilled onClick={() => console.log('click')}/>
    </div>
);

export default Basic;
