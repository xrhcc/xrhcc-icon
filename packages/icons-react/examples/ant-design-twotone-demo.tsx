import * as React from 'react';
import {AccountBookFilled} from '../src';

export default () => (
    <div className="icons-list">
        <AccountBookFilled/>
        <AccountBookFilled twoToneColor="#eb2f96"/>
        <AccountBookFilled/>
        <AccountBookFilled twoToneColor="#52c41a"/>
        <AccountBookFilled twoToneColor={['#52c41a', 'transparent']}/>
    </div>
);
