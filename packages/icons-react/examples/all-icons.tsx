import {ThemeType} from '@xrhcc-ui/icons-svg/lib/types';
import * as React from 'react';
import styled from 'styled-components';
import * as XrhccIcons from '../src/icons';
import {Button} from "antd";
import { AimingBase,AddFourEditor } from '../src/icons';

import 'antd/dist/antd.less'

const Container = styled.div`
  display: flex;
  flex-flow: row wrap;
  width: 80vw;
  margin: auto;
  color: red;
`;

const Card = styled.div`
  height: 90px;
  margin: 12px 0 16px;
  width: 20%;
  text-align: center;
`;

const NameDescription = styled.p`
  display: block;
  text-align: center;
  transform: scale(0.83);
  font-family: 'Lucida Console', Consolas;
  white-space: nowrap;
`;

const allIcons: {
    [key: string]: any;
} = XrhccIcons;

const AllIconDemo = () => {
    const [currentTheme, setCurrentTheme] = React.useState('Antd');

    const handleSelectChange = React.useCallback((e: React.ChangeEvent<HTMLSelectElement>) => {
        const value = e.currentTarget.value as ThemeType;
        setCurrentTheme(value);
    }, []);

    console.log(allIcons)
    const visibleIconList = React.useMemo(
        () => Object.keys(allIcons).filter(iconName => {
            console.log(visibleIconList)
            return iconName.includes(currentTheme)}),
        [currentTheme],
    );



    return (
        <div style={{color: '#555'}}>
            <Button icon={<AimingBase />}>ddd</Button>

            <Button icon={<AddFourEditor />}>ddd</Button>

            {/*<Button*/}
            {/*    type="primary"*/}
            {/*    icon={<CaretUpFill />}*/}
            {/*>*/}
            {/*    Click me!*/}
            {/*</Button>*/}

            <h1 style={{textAlign: 'center'}}>All Icons</h1>
            <div style={{textAlign: 'center'}}>
                <select
                    name="theme-select"
                    value={currentTheme}
                    onChange={handleSelectChange}>

                    <option value="Antd">Antd</option>
                    <option value="Assets">Assets</option>
                    <option value="Base">Base</option>
                    <option value="Chart">Chart</option>
                    <option value="Chain">Chain</option>
                    <option value="Data">Data</option>
                    <option value="Date">Date</option>
                    <option value="Direction">Direction</option>
                    <option value="Editor">Editor</option>
                    <option value="Flow">Flow</option>
                    <option value="Gesture">Gesture</option>
                    <option value="Graphics">Graphics</option>
                    <option value="Hardware">Hardware</option>
                    <option value="Health">Health</option>
                    <option value="Icons">Icons</option>
                    <option value="Link">Link</option>
                    <option value="Office">Office</option>
                    <option value="Other">Other</option>
                    <option value="Safe">Safe</option>
                    <option value="Screen">Screen</option>
                    <option value="Symbol">Symbol</option>
                    <option value="Telecom">Telecom</option>
                    <option value="User">User</option>
                    <option value="Weather">Weather</option>

                </select>
            </div>
            <Container>
                {
                    visibleIconList.map(iconName => {
                        const Component = allIcons[iconName];
                        return (
                            <Card key={iconName}>
                                <Component style={{fontSize: '24px'}}/>
                                <NameDescription>{iconName}</NameDescription>
                            </Card>
                        );
                    })
                }
            </Container>
        </div>
    );
}

export default AllIconDemo;
