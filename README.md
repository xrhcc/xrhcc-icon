<p align="center">
<a href="https://yqplatform.com/">
    <img src="static/images/logo.png" width="200px" height="auto" alt="logo">
</a>
</p>



<p align="center">
⭐ The abstract trees of the Xrhcc SVG icons.
</p>

## Packages
- Svg: [@xrhcc-ui/icons-svg](./packages/icons-svg) 
- React: [@xrhcc-ui/icons](./packages/icons-react) 
- Vue: [@xrhcc-ui/icons-vue](./packages/icons-vue)


## Contribution Guides 贡献指南

- @xrhcc-ui/icons-svg: [English](./packages/icons-svg/docs/ContributionGuide.md) ｜ [中文](./packages/icons-svg/docs/ContributionGuide.zh-CN.md)


## License

[MIT License](./LICENSE)
